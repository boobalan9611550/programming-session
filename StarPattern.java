 /*public class StarPattern
{
	public static void main(String []args)
	{
		for(int no=5;no>=1;no--)   // row print
		{
			for(int row =1;row<=no;row++) 
			{
				System.out.print("*");
			}
			   System.out.println();
		}
	}
}*/

/*public class StarPattern {
    public static void main(String[] args) {
        int rows = 5; // Change the number of rows as needed

        for (int i = 1; i <= rows; i++) {
            // Print spaces before the stars
            for (int j = 1; j <= rows - i; j++) {
                System.out.print(" ");
            }

            // Print stars
            for (int k = 1; k <= 2 * i - 1; k++) {
                System.out.print("*");
            }

            // Move to the next line after each row
            System.out.println();
        }
    }
}*/


/*public class StarPattern {
    public static void main(String[] args) {
		for(int i=5;i>=1;i--)
		{
			for( int j=1;j<=i;j++)
			{
				System.out.print(j);
			}
			System.out.println();
		}
	}
}

output;
12345
1234
123
12
1  */


public class StarPattern {
    public static void main(String[] args) {
		for(int i=1;i<=5;i++)
		{
			for( int j=1;j<=i;j++)
			{
				System.out.print(j);
			}
			System.out.println();
		}
	}
}


/*output
1
12
123
1234
12345 */


