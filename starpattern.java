package starpattern;

public class starpattern {
	public static void main(String[] args) {
		starpattern sp = new starpattern();
		sp.Normalstar();
		sp.starorder();
		sp.starreverse();
		sp.reversenumber();
		sp.ordernumber();
		sp.lastrowstar();

	}

	private void Normalstar() {
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= 5; col++) {
				System.out.print("*");
			}
			System.out.println();
		}
		System.out.println("---------------Normalstar---------------------");
	}

	private void starorder() {
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print("*");
			}
			System.out.println();
		}
		System.out.println("------------------starorder --------------------");

	}

	private void starreverse() {
		for (int row = 5; row >= 1; row--) {
			// 5
			for (int col = 1; col <= row; col++) {

				System.out.print("*");
			}

			System.out.println();
		}

		System.out.println("-------------------------starreverse----------------------");
	}

	private void reversenumber() {
		for (int row = 5; row >= 1; row--) {

			for (int col = 1; col <= row; col++) {

				System.out.print(col);
			}

			System.out.println();
		}

		System.out.println("------------------------reversenumber-----------------------");
	}

	private void ordernumber() {
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print(col);
			}
			System.out.println();
		}
		System.out.println("------------------ordernumber --------------------");
	}

	private void lastrowstar() {
		for (int row = 1; row <= 5; row++) {

			for (int col = 1; col < 6-row; col++) {

				System.out.print(col+"");
			}

			System.out.println("*");
		}

		System.out.println("------------------------lastrowstar-----------------------");		
	}
	
	
}
